//
//  ApiEndPoints.swift
//  SampleApp
//
//  Created by Purushotam on 25/08/17.
//  Copyright © 2017 Purushotam. All rights reserved.
//

import Foundation
//import Alamofire

public enum ApiEndPoints {
    
    case getPopularMovies()
    case getUpcommingMovies()
    
    
    // MARK: - Public Properties
    var method: String {
        switch self {
        case .getPopularMovies():
            return "GET"
        case .getUpcommingMovies():
            return "GET"
        }
    }
    
    var url : URL {
        let baseUrl = URL.getBaseUrl()
        switch self {
        case .getPopularMovies():
            return baseUrl.appendingPathComponent("movie/popular?api_key=d4d960ef29da89622aa33e6af38b07e9")
        case .getUpcommingMovies():
            return baseUrl.appendingPathComponent("movie/popular?api_key=d4d960ef29da89622aa33e6af38b07e9")
        }
    }
    
}

// get base url from info plist file
fileprivate extension  URL {
    static func getBaseUrl() -> URL {
        //        guard let info = Bundle.main.infoDictionary, let urlString = info["Base Url"] as? String,
        //        let url = URL(string:urlString) else {
        //            fatalError("Cannot get base url from info.pilist")
        //        }
        
        let urlString = "https://api.themoviedb.org/3/"
        
        guard let url = URL(string:urlString) else {
            fatalError("Cannot get base url from info.pilist")
        }
        
        return url
    }
}
