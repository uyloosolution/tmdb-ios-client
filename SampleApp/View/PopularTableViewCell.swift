//
//  PopularTableViewCell.swift
//  SampleApp
//
//  Created by Purushotam on 25/08/17.
//  Copyright © 2017 Purushotam. All rights reserved.
//

import UIKit

class PopularTableViewCell: UITableViewCell {

    @IBOutlet  var collectionView: UICollectionView!
    var dataSourceArray = [PopularDataModel]()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension PopularTableViewCell : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSourceArray.count
    }
    
    @available(iOS 6.0, *)
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : PopularCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell",
                                                                                  for: indexPath as IndexPath) as! PopularCollectionViewCell
        
        let wrapper = dataSourceArray[indexPath.row]
        if let path = wrapper.backdrop_path {
            let urlString = "http://image.tmdb.org/t/p/w500" + path
            let url = URL(string: urlString)
            cell.MovieImg.downloadedFrom(url: url!, contentMode: .scaleToFill)
            cell.MovieImg.clipsToBounds = true
        }
        cell.MovieName.text = wrapper.title
        
        return cell
    }
    
    /**
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        let screenSize: CGRect = UIScreen.main.bounds
        let screenwidth = screenSize.width
        return CGSize.init(width:screenwidth-30, height: 211.5)
    }
 */
    
}
