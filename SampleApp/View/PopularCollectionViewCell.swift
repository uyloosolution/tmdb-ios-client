//
//  PopularCollectionViewCell.swift
//  SampleApp
//
//  Created by Purushotam on 25/08/17.
//  Copyright © 2017 Purushotam. All rights reserved.
//

import UIKit

class PopularCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var MovieImg: UIImageView!
    @IBOutlet weak var MovieName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
