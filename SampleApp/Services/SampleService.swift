//
//  SampleService.swift
//  SampleApp
//
//  Created by Purushotam on 25/08/17.
//  Copyright © 2017 Purushotam. All rights reserved.
//

import Foundation

typealias QueryResult = ([PopularDataModel]?, String) -> ()
typealias JSONDictionary = [String: Any]


class SampleService {

    typealias JSONDictionary = [String: Any]

    let defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    var errorMessage = ""
    
    var popularVideos = [PopularDataModel]()

    public func getPopularMovies(completion: @escaping (ApiResult<Any>) -> Void)  {

       // dataTask?.cancel()
        // 2
        if var urlComponents = URLComponents(string: "https://api.themoviedb.org/3/movie/popular") {
            urlComponents.query = "api_key=d4d960ef29da89622aa33e6af38b07e9"

            guard let url = urlComponents.url else { return }

            dataTask = defaultSession.dataTask(with: url) { data, response, error in
                defer { self.dataTask = nil }

                guard error == nil else {
                   
                    DispatchQueue.main.async {
                        completion(.failure(error!))
                    }
                    return
                }
                
                if let data = data,let response = response as? HTTPURLResponse, response.statusCode == 200 {
                    
                    let jsonData = self.parseData(data)
                    if let parseError = (self.parseData(data)).1 {
                        DispatchQueue.main.async {
                            completion(.failure(parseError))
                        }
                        return
                    }
                    
                    DispatchQueue.main.async {
                        completion(.success(self.convertToNativeObjects(jsonData.0)))
                    }
                    return
                }
                
                DispatchQueue.main.async {
                    completion(.failure(NSError(domain: "com.sample.client", code: 1001, userInfo: [NSLocalizedDescriptionKey : "Unknown error occured"])))
                }
            }

            dataTask?.resume()
        }
    }
    
    public func getUpComingMoviesMovies(completion: @escaping (ApiResult<Any>) -> Void)  {
        
       // dataTask?.cancel()

        if var urlComponents = URLComponents(string: "http://api.themoviedb.org/3/movie/upcoming") {
            urlComponents.query = "api_key=d4d960ef29da89622aa33e6af38b07e9"
            
            guard let url = urlComponents.url else { return }
            
            dataTask = defaultSession.dataTask(with: url) { data, response, error in
                defer { self.dataTask = nil }
                
                guard error == nil else {
                    DispatchQueue.main.async {
                        completion(.failure(error!))
                    }
                    return
                }
                
                if let data = data,let response = response as? HTTPURLResponse, response.statusCode == 200 {
                   
                    let jsonData = self.parseData(data)
                    if let parseError = (self.parseData(data)).1 {
                        DispatchQueue.main.async {
                            completion(.failure(parseError))
                        }
                        return
                    }
                    
                    DispatchQueue.main.async {
                        completion(.success(self.convertToNativeObjects(jsonData.0)))
                    }
                    return
                }
                
                DispatchQueue.main.async {
                    completion(.failure(NSError(domain: "com.sample.client", code: 1001, userInfo: [NSLocalizedDescriptionKey : "Unknown error occured"])))
                }
            }
            
            dataTask?.resume()
        }
    }

    
    public func getTopRatedMovies(completion: @escaping (ApiResult<Any>) -> Void)  {
        
       // dataTask?.cancel()
        
      
        if var urlComponents = URLComponents(string: "https://api.themoviedb.org/3/movie/top_rated") {
            urlComponents.query = "api_key=d4d960ef29da89622aa33e6af38b07e9&language=en-US&page=1"
            
            guard let url = urlComponents.url else { return }
            
            dataTask = defaultSession.dataTask(with: url) { data, response, error in
                defer { self.dataTask = nil }
                
                guard error == nil else {
                    DispatchQueue.main.async {
                        completion(.failure(error!))
                    }
                    return
                }
                
                if let data = data,let response = response as? HTTPURLResponse, response.statusCode == 200 {
                    
                    let jsonData = self.parseData(data)
                    if let parseError = (self.parseData(data)).1 {
                        DispatchQueue.main.async {
                            completion(.failure(parseError))
                        }
                        return
                    }
                    
                    DispatchQueue.main.async {
                        completion(.success(self.convertToNativeObjects(jsonData.0)))
                    }
                    return
                }
                
                DispatchQueue.main.async {
                    completion(.failure(NSError(domain: "com.sample.client", code: 1001, userInfo: [NSLocalizedDescriptionKey : "Unknown error occured"])))
                }
            }
            
            dataTask?.resume()
        }
    }
    
    fileprivate func parseData(_ data: Data) -> (JSONDictionary? ,NSError?) {

        var response: JSONDictionary?
        
        do {
            response = try JSONSerialization.jsonObject(with: data, options: []) as? JSONDictionary
        } catch let parseError as NSError {
            return (response, parseError)
        }
        
        return (response, nil)
    }
    
    fileprivate func convertToNativeObjects(_ response: JSONDictionary?) -> [PopularDataModel] {

        var nativeObjects = [PopularDataModel]()

        guard let array = response!["results"] as? [Any] else {
            print("Problem parsing trackDictionary")
            return nativeObjects
        }
        
        for trackDictionary in array     {

            if let trackDictionary = trackDictionary as? JSONDictionary{
                
                let dataModel = PopularDataModel.init(json: trackDictionary)
                nativeObjects.append(dataModel!)
                
            } else {
                print("Problem parsing trackDictionary")
            }
        }
        
        return nativeObjects
    }
}
