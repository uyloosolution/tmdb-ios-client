//
//  HomeViewController.swift
//  SampleApp
//
//  Created by Purushotam on 25/08/17.
//  Copyright © 2017 Purushotam. All rights reserved.
//

import UIKit

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFill) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
    }
    
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFill) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}

class HomeViewController: UIViewController {

    let simpleService = SampleService()
    let downloadService = DownloadService()

    let headersTitle = ["New in Theatrees","Popular", "Highest Rated This Year"]
    @IBOutlet weak var tableView: UITableView!

    // Mark : lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.getPopularMovies()
        self.getUpComingMovies()
        self.getopRatedMovies()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // Mark : Private Methods
    
    func getPopularMovies() {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        simpleService.getPopularMovies { result in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            guard result.error == nil else {
                print(result.error!)
                return
            }
            
            if let polularVideos = result.value as? [PopularDataModel] {
                if let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? PopularTableViewCell {
                    cell.dataSourceArray = polularVideos
                    cell.collectionView.reloadData()
                }
            }
        }
    }
    
    func getUpComingMovies() {

        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        simpleService.getUpComingMoviesMovies { result in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            guard result.error == nil else {
                print(result.error!)
                return
            }
            
            if let polularVideos = result.value as? [PopularDataModel] {
                if let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 1)) as? PopularTableViewCell {
                    cell.dataSourceArray = polularVideos
                    cell.collectionView.reloadData()
                }
            }
        }    
    }
    
    
    func getopRatedMovies() {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        simpleService.getTopRatedMovies { result in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            guard result.error == nil else {
                print(result.error!)
                return
            }
            
            if let polularVideos = result.value as? [PopularDataModel] {
                if let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 2)) as? PopularTableViewCell {
                    cell.dataSourceArray = polularVideos
                    cell.collectionView.reloadData()
                }
            }
        }
    }
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension HomeViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return self.tableView.frame.height/3
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return headersTitle[section]
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:PopularTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "popularcell") as! PopularTableViewCell

        cell.collectionView.tag = indexPath.row
        return cell
    }
    
 
}

extension HomeViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
