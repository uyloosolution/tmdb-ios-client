//
//  PopularDataModel.swift
//  SampleApp
//
//  Created by Purushotam on 25/08/17.
//  Copyright © 2017 Purushotam. All rights reserved.
//

import Foundation
/*
["poster_path": /q0R4crx2SehcEEQEkYObktdeFy.jpg, "backdrop_path": /uX7LXnsC7bZJZjn048UCOwkPXWJ.jpg, "genre_ids": <__NSArrayI 0x60400064cfc0>(
    10751,
    16,
    12,
    35
    )
    , "vote_count": 4185, "overview": Minions Stuart, Kevin and Bob are recruited by Scarlet Overkill, a super-villain who, alongside her inventor husband Herb, hatches a plot to take over the world., "original_title": Minions, "vote_average": 6.4, "popularity": 215.441555, "id": 211672, "original_language": en, "release_date": 2015-06-17, "video": 0, "title": Minions, "adult": 0]
*/
class PopularDataModel {
    
    let vote_count: UInt
    let id: NSNumber
    let video: Bool
    let vote_average : Float
    let title : String
    let popularity : Float
    let poster_path : String
    let original_language : String
    let original_title : String
    let backdrop_path : String?
    let adult : Bool
    let overview : String
    let release_date : String
    
    init?(json: [String: Any]) {

        vote_count = json["vote_count"] as! UInt
        id = json["id"] as! NSNumber
        video = json["video"] as! Bool
        vote_average = json["vote_average"] as! Float
        title = json["title"] as! String
        popularity = json["popularity"] as! Float
        poster_path = json["poster_path"] as! String
        original_language = json["original_language"] as! String
        original_title = json["original_title"] as! String
        backdrop_path = json["backdrop_path"] as? String
        adult = json["adult"] as! Bool
        overview = json["overview"] as! String
        release_date = json["release_date"] as! String

    }
    
    init(vote_count: UInt, id: NSNumber, video: Bool,vote_average : Float, title: String,popularity:Float,poster_path:String,original_language:String,original_title:String,backdrop_path:String?,adult:Bool,overview:String,release_date:String) {
        
        self.vote_count = vote_count
        self.id = id
        self.video = video
        self.vote_average = vote_average
        self.title = title
        self.popularity = popularity
        self.poster_path = poster_path
        self.original_language = original_language
        self.original_title = original_title
        self.backdrop_path = backdrop_path
        self.adult = adult
        self.overview = overview
        self.release_date = release_date
        
    }
    
}
